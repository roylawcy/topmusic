package com.roylaw.app.topmusic.data;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

/**
 * Song object class, also use as the database entity.
 */
@Entity
public class Song {

    @Id
    public long id;             // local object id

    public String name;         // song name
    public String artistName;   // artist name
    public String artworkUrl;   // artwork url

    public Song() {}

    public Song(String name, String artistName, String artworkUrl) {
        this.name = name;
        this.artistName = artistName;
        this.artworkUrl = artworkUrl;
    }
}
