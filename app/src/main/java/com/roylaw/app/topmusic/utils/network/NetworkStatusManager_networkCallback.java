package com.roylaw.app.topmusic.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkRequest;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.roylaw.app.topmusic.application.MyApplication;

/**
 * This NetworkStatusManager is for version >= Lollipop.
 */
public class NetworkStatusManager_networkCallback extends BaseNetworkStatusManager implements NetworkStatusManager {

    private ConnectivityManager mConnectivityManager;

    private ConnectivityManager.NetworkCallback mNetworkCallback;

    public NetworkStatusManager_networkCallback(MyApplication application) {
        mConnectivityManager = (ConnectivityManager) application.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void register(Context context, final NetworkCallback networkCallback) {

        mNetworkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                super.onAvailable(network);
                networkCallback.onNetworkAvailable();
            }

            @Override
            public void onLost(Network network) {
                super.onLost(network);
                networkCallback.onNetworkUnavailable();
            }

            @Override
            public void onUnavailable() {
                super.onUnavailable();
            }
        };

        NetworkRequest networkRequest = new NetworkRequest.Builder().build();
        mConnectivityManager.registerNetworkCallback(networkRequest, mNetworkCallback);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void unregister(Context context) {
        mConnectivityManager.unregisterNetworkCallback(mNetworkCallback);
    }
}
