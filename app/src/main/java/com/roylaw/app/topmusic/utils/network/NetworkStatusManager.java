package com.roylaw.app.topmusic.utils.network;

import android.content.Context;

public interface NetworkStatusManager  {

    interface NetworkCallback {
        void onNetworkAvailable();
        void onNetworkUnavailable();
    }

    /**
     * Register to monitor network changes.
     * @param context context
     * @param networkCallback callback
     */
    void register(Context context, NetworkCallback networkCallback);

    /**
     * Unregister the monitoring of network changes.
     * @param context context, must be the same as the one in register().
     */
    void unregister(Context context);
}
