package com.roylaw.app.topmusic.base;

public interface BasePresenter {

    void start();
    void subscribeView(BaseView view);
    void unsubscribeView();
}
