package com.roylaw.app.topmusic.utils.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class BaseNetworkStatusManager implements NetworkStatusManager {

    /**
     * Check whether network is available.
     * @param context context
     * @return true if networkk available, false otherwise.
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;

        // determinate whether network is available
        return networkInfo != null && networkInfo.isAvailable();
    }

    @Override
    public void register(Context context, NetworkCallback networkCallback) {
    }

    @Override
    public void unregister(Context context) {
    }
}
