package com.roylaw.app.topmusic.data.source.remote.response;

import com.google.gson.annotations.SerializedName;

public class ApiResponse {

    @SerializedName("feed")
    public Feed feed;

}
