package com.roylaw.app.topmusic.songs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.roylaw.app.topmusic.R;
import com.roylaw.app.topmusic.base.BaseFragment;
import com.roylaw.app.topmusic.data.Song;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SongsFragment extends BaseFragment implements SongsContract.View {
    public static final String TAG = SongsFragment.class.getName();

    // Views
    @BindView(R.id.root_layout)
    CoordinatorLayout mRootLayout;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.refresh_button)
    FloatingActionButton mRefreshButton;

    @BindView(R.id.loading_view)
    ProgressBar mLoadingView;

    private SongsContract.Presenter mPresenter;

    private SongsAdapter mAdapter;

    public static SongsFragment newInstance() {

        Bundle args = new Bundle();

        SongsFragment fragment = new SongsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setPresenter(SongsContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_songs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // initialize recycler view
        setupRecyclerView();

        mPresenter.subscribeView(this);

        // start monitoring network status
        mPresenter.startMonitoringNetworkStatus();

        // start presenter
        mPresenter.start();
    }

    @Override
    public void onResume() {
        super.onResume();

        // always check the network status at onResume()
        mPresenter.checkNetworkStatus();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        // stop monitoring network status
        mPresenter.stopMonitoringNetworkStatus();

        mPresenter.unsubscribeView();
    }

    @Override
    public void showSongs(List<Song> songs) {
        mAdapter.replaceData(songs);
    }

    @Override
    public void scrollToTop() {
        mRecyclerView.smoothScrollToPosition(0);
    }

    @Override
    public void enableRefreshButton() {
        mRefreshButton.setEnabled(true);
    }

    @Override
    public void disableRefreshButton() {
        mRefreshButton.setEnabled(false);
    }

    @Override
    public void startLoading() {
        mRefreshButton.hide();
        mLoadingView.setVisibility(View.VISIBLE);
        mAdapter.clearData();
    }

    @Override
    public void stopLoading() {
        mRefreshButton.show();
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(int messageResId) {
        Snackbar.make(mRootLayout, messageResId, Snackbar.LENGTH_LONG).show();
    }

    @OnClick(R.id.refresh_button)
    public void onRefreshButtonClicked(View button) {
        mPresenter.loadSongs(true);
    }

    private void setupRecyclerView() {
        mAdapter = new SongsAdapter();

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setAdapter(mAdapter);
    }
}
