package com.roylaw.app.topmusic.songs;

import com.roylaw.app.topmusic.application.di.ApiModule;
import com.roylaw.app.topmusic.application.di.DatabaseModule;
import com.roylaw.app.topmusic.data.SongsRepository;
import com.roylaw.app.topmusic.data.source.local.SongsLocalDataSource;
import com.roylaw.app.topmusic.data.source.remote.SongsRemoteDataSource;
import com.roylaw.app.topmusic.utils.network.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.objectbox.BoxStore;

@Module(includes = {DatabaseModule.class, ApiModule.class})
public class SongsModule {

    @Singleton
    @Provides
    public SongsRepository provideSongsRepository(SongsRemoteDataSource remoteDataSource,
                                                  SongsLocalDataSource localDataSource) {
        SongsRepository songsRepository = new SongsRepository(remoteDataSource, localDataSource);
        return songsRepository;
    }

    @Singleton
    @Provides
    public SongsRemoteDataSource provideRemoteDataSource(ApiService apiService) {
        SongsRemoteDataSource remoteDataSource = new SongsRemoteDataSource(apiService);
        return remoteDataSource;
    }

    @Singleton
    @Provides
    public SongsLocalDataSource provideLocalDataSource(BoxStore boxStore) {
        SongsLocalDataSource localDataSource = new SongsLocalDataSource(boxStore);
        return localDataSource;
    }
}
