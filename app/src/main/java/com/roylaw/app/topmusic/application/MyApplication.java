package com.roylaw.app.topmusic.application;

import android.app.Application;

import com.roylaw.app.topmusic.application.di.ApiModule;
import com.roylaw.app.topmusic.application.di.AppComponent;
import com.roylaw.app.topmusic.application.di.AppModule;
import com.roylaw.app.topmusic.application.di.DaggerAppComponent;
import com.roylaw.app.topmusic.application.di.DatabaseModule;
import com.roylaw.app.topmusic.application.di.NetworkModule;
import com.roylaw.app.topmusic.songs.SongsModule;
import com.roylaw.app.topmusic.utils.Configs;

public class MyApplication extends Application {

    private static MyApplication mInstance;
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;

        // setup app component
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .apiModule(new ApiModule(Configs.API_URL))
                .databaseModule(new DatabaseModule())
                .songsModule(new SongsModule())
                .build();
    }

    public static MyApplication getInstance() {
        return mInstance;
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
