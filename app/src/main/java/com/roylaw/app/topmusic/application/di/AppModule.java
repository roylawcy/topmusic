package com.roylaw.app.topmusic.application.di;

import com.roylaw.app.topmusic.application.MyApplication;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private MyApplication mApplication;

    public AppModule(MyApplication myApplication) {
        mApplication = myApplication;
    }

    @Provides
    public MyApplication provideBaseApplication() {
        return mApplication;
    }
}
