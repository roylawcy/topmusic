package com.roylaw.app.topmusic.data.source.remote;

import android.support.annotation.NonNull;

import com.roylaw.app.topmusic.data.Song;
import com.roylaw.app.topmusic.data.source.SongsDataSource;
import com.roylaw.app.topmusic.data.source.remote.response.ApiResponse;
import com.roylaw.app.topmusic.data.source.remote.response.MediaResult;
import com.roylaw.app.topmusic.utils.Configs;
import com.roylaw.app.topmusic.utils.network.ApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Remote data source for song. Connected to iTunes api.
 */
public class SongsRemoteDataSource implements SongsDataSource {

    public ApiService mApiService;

    public SongsRemoteDataSource(ApiService apiService) {
        mApiService = apiService;
    }

    @Override
    public void getSongs(@NonNull final GetSongsCallback callback) {
        // prepare request
        Call<ApiResponse> call = mApiService.getMedias(
                Configs.COUNTRY_HONG_KONG,
                Configs.MEDIA_TYPE_ITUNES_MUSIC,
                Configs.FEED_TYPE_TOP_SONGS,
                Configs.GENRE_ALL,
                Configs.RESULTS_LIMIT,
                Configs.ALLOW_EXPLICIT_YES);

        // send request
        call.enqueue(new Callback<ApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<ApiResponse> call, @NonNull Response<ApiResponse> response) {
                // error checking
                if (!response.isSuccessful() || response.body() == null) {
                    onFailure(call, new Throwable());
                    return;
                }

                ApiResponse apiResponse = response.body();

                List<Song> songs = new ArrayList<>();

                // convert api responses to song entities
                assert apiResponse != null;
                for (MediaResult mediaResult : apiResponse.feed.results) {
                    Song song = mediaResult.toSongEntity();
                    if (song != null) {
                        songs.add(song);
                    }
                }

                // return songs via callback
                callback.onSongsLoaded(songs);
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse> call, @NonNull Throwable t) {
                // data not available
                callback.onDataNotAvailable();
            }
        });
    }

    @Override
    public void deleteAllSongs() {
        // not applicable for remote source
    }

    @Override
    public void saveSong(Song song) {
        // not applicable for remote source
    }
}
