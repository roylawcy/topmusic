package com.roylaw.app.topmusic.application.di;

import com.roylaw.app.topmusic.BuildConfig;
import com.roylaw.app.topmusic.application.MyApplication;
import com.roylaw.app.topmusic.data.MyObjectBox;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;

@Module(includes = {AppModule.class})
public class DatabaseModule {

    public DatabaseModule() {

    }

    @Singleton
    @Provides
    public BoxStore provideBoxStore(MyApplication myApplication) {
        BoxStore boxStore = MyObjectBox.builder().androidContext(myApplication).build();

        if (BuildConfig.DEBUG) {
            new AndroidObjectBrowser(boxStore).start(myApplication);
        }

        return boxStore;
    }
}
