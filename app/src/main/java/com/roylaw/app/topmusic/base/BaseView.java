package com.roylaw.app.topmusic.base;

public interface BaseView<T> {

    void setPresenter(T presenter);
}
