package com.roylaw.app.topmusic.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(View itemView) {
        super(itemView);

        // bind view with ButterKnife
        ButterKnife.bind(this, itemView);
    }
}
