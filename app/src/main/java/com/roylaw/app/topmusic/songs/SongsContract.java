package com.roylaw.app.topmusic.songs;

import com.roylaw.app.topmusic.base.BasePresenter;
import com.roylaw.app.topmusic.base.BaseView;
import com.roylaw.app.topmusic.data.Song;

import java.util.List;

/**
 * Define View and Presenter in Contract class.
 */
public interface SongsContract {

    interface View extends BaseView<Presenter> {
        void showSongs(List<Song> songs);
        void scrollToTop();
        void enableRefreshButton();
        void disableRefreshButton();
        void startLoading();
        void stopLoading();
        void showMessage(int messageResId);
    }

    interface Presenter extends BasePresenter {
        void loadSongs(boolean fromRemote);
        void startMonitoringNetworkStatus();
        void stopMonitoringNetworkStatus();
        void checkNetworkStatus();
    }
}
