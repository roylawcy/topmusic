package com.roylaw.app.topmusic.utils.network;

import com.roylaw.app.topmusic.data.source.remote.response.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {

    @GET("{country}/{mediaType}/{feedType}/{genre}/{resultsLimit}/{allowExplicit}.json")
    Call<ApiResponse> getMedias(@Path("country") String country,
                                @Path("mediaType") String mediaType,
                                @Path("feedType") String feedType,
                                @Path("genre") String genre,
                                @Path("resultsLimit") int resultsLimit,
                                @Path("allowExplicit") String allowExplicit);

}
