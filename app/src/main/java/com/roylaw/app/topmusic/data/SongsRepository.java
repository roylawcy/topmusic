package com.roylaw.app.topmusic.data;

import android.support.annotation.NonNull;

import com.roylaw.app.topmusic.data.source.SongsDataSource;
import com.roylaw.app.topmusic.data.source.local.SongsLocalDataSource;
import com.roylaw.app.topmusic.data.source.remote.SongsRemoteDataSource;

import java.util.List;

public class SongsRepository {

    private SongsRemoteDataSource mRemoteDataSource;
    private SongsLocalDataSource mLocalDataSource;

    public SongsRepository(SongsRemoteDataSource remoteDataSource,
                           SongsLocalDataSource localDataSource) {
        mRemoteDataSource = remoteDataSource;
        mLocalDataSource = localDataSource;
    }

    public void getSongs(boolean fromRemote, @NonNull final SongsDataSource.GetSongsCallback callback) {
        if (fromRemote) {
            getSongsFromRemote(callback);
        } else {
            getSongsFromLocal(callback);
        }
    }

    private void getSongsFromLocal(@NonNull final SongsDataSource.GetSongsCallback callback) {
        mLocalDataSource.getSongs(callback);
    }

    private void getSongsFromRemote(@NonNull final SongsDataSource.GetSongsCallback callback) {
        mRemoteDataSource.getSongs(new SongsDataSource.GetSongsCallback() {
            @Override
            public void onSongsLoaded(List<Song> songs) {
                updateLocalDataSource(songs);
                callback.onSongsLoaded(songs);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }
        });
    }

    private void updateLocalDataSource(List<Song> songs) {
        mLocalDataSource.deleteAllSongs();
        for (Song song : songs) {
            mLocalDataSource.saveSong(song);
        }
    }
}
