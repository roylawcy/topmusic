package com.roylaw.app.topmusic.data.source;

import android.support.annotation.NonNull;

import com.roylaw.app.topmusic.data.Song;

import java.util.List;

public interface SongsDataSource {
    interface GetSongsCallback {
        void onSongsLoaded(List<Song> songs);
        void onDataNotAvailable();
    }

    void getSongs(@NonNull GetSongsCallback callback);

    void deleteAllSongs();

    void saveSong(Song song);
}
