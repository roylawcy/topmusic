package com.roylaw.app.topmusic.data.source.local;

import android.support.annotation.NonNull;

import com.roylaw.app.topmusic.data.Song;
import com.roylaw.app.topmusic.data.source.SongsDataSource;

import java.util.List;

import io.objectbox.Box;
import io.objectbox.BoxStore;

/**
 * Song local data source, implemented with ObjectBox.
 */
public class SongsLocalDataSource implements SongsDataSource {

    public BoxStore mBoxStore;

    public SongsLocalDataSource(BoxStore boxStore) {
        mBoxStore = boxStore;
    }

    @Override
    public void getSongs(@NonNull GetSongsCallback callback) {
        Box<Song> songBox = mBoxStore.boxFor(Song.class);

        // get all songs from database and callback
        List<Song> songs = songBox.getAll();
        callback.onSongsLoaded(songs);
    }

    @Override
    public void deleteAllSongs() {
        Box<Song> songBox = mBoxStore.boxFor(Song.class);

        // remove all songs
        songBox.removeAll();
    }

    @Override
    public void saveSong(Song song) {
        Box<Song> songBox = mBoxStore.boxFor(Song.class);

        // save song to database
        songBox.put(song);
    }
}
