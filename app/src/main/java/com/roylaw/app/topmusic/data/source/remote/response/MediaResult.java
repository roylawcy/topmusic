package com.roylaw.app.topmusic.data.source.remote.response;

import com.google.gson.annotations.SerializedName;
import com.roylaw.app.topmusic.data.Song;

public class MediaResult {
    private static final String KIND_SONG = "song";

    @SerializedName("name")
    public String name;

    @SerializedName("kind")
    public String kind;

    @SerializedName("artistName")
    public String artistName;

    @SerializedName("artworkUrl100")
    public String artworkUrl;

    /**
     * Convert media result to song entity object
     * @return new Song instance
     */
    public Song toSongEntity() {
        if (kind.equals(KIND_SONG)) {
            Song song = new Song();
            song.name = name;
            song.artistName = artistName;
            song.artworkUrl = artworkUrl;

            return song;

        } else {
            return null;
        }
    }
}
