package com.roylaw.app.topmusic.utils.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * This NetworkStatusManager is for version < Lollipop.
 */
public class NetworkStatusManager_broadcast extends BaseNetworkStatusManager implements NetworkStatusManager {

    private BroadcastReceiver mNetworkReceiver;

    public NetworkStatusManager_broadcast() {
    }

    @Override
    public void register(Context context, final NetworkCallback networkCallback) {
        // setup receiver
        mNetworkReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // determinate whether network is available
                if (isNetworkAvailable(context)) {
                    Log.d("NetworkStatusManager", "onNetworkAvailable");
                    networkCallback.onNetworkAvailable();
                } else {
                    Log.d("NetworkStatusManager", "onNetworkUnavailable");
                    networkCallback.onNetworkUnavailable();
                }
            }
        };

        // register receiver
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(mNetworkReceiver, intentFilter);
    }

    @Override
    public void unregister(Context context) {
        context.unregisterReceiver(mNetworkReceiver);
    }

}
