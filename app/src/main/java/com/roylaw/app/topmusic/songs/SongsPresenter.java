package com.roylaw.app.topmusic.songs;

import android.support.annotation.NonNull;

import com.roylaw.app.topmusic.R;
import com.roylaw.app.topmusic.application.MyApplication;
import com.roylaw.app.topmusic.base.BaseView;
import com.roylaw.app.topmusic.data.Song;
import com.roylaw.app.topmusic.data.SongsRepository;
import com.roylaw.app.topmusic.data.source.SongsDataSource;
import com.roylaw.app.topmusic.utils.network.BaseNetworkStatusManager;
import com.roylaw.app.topmusic.utils.network.NetworkStatusManager;

import java.util.List;

import javax.inject.Inject;

public class SongsPresenter implements SongsContract.Presenter {

    private SongsContract.View mSongsView;

    private SongsRepository mSongsRepository;

    @Inject
    public BaseNetworkStatusManager mNetworkStatusManager;

    public SongsPresenter(@NonNull SongsRepository songsRepository) {
        mSongsRepository = songsRepository;

        // inject dependencies
        MyApplication.getInstance().getAppComponent().inject(this);
    }

    @Override
    public void start() {
        loadSongs(false);
    }

    @Override
    public void subscribeView(BaseView view) {
        mSongsView = (SongsContract.View) view;
        mSongsView.setPresenter(this);
    }

    @Override
    public void unsubscribeView() {
        mSongsView = null;
    }

    @Override
    public void loadSongs(final boolean fromRemote) {
        if (mSongsView == null) return;

        if (fromRemote) {
            // start loading
            mSongsView.startLoading();
        }

        mSongsRepository.getSongs(fromRemote, new SongsDataSource.GetSongsCallback() {
            @Override
            public void onSongsLoaded(List<Song> songs) {
                if (mSongsView == null) return;

                // show songs in view
                mSongsView.showSongs(songs);

                // if new songs are loaded from remote
                if (fromRemote) {
                    // scroll to top
                    mSongsView.scrollToTop();

                    // show success message
                    mSongsView.showMessage(R.string.message_refresh_success);

                    // stop loading
                    mSongsView.stopLoading();
                }
            }

            @Override
            public void onDataNotAvailable() {
                if (mSongsView == null) return;

                // show error message
                mSongsView.showMessage(R.string.message_data_not_available);

                if (fromRemote) {
                    // stop loading
                    mSongsView.stopLoading();
                }
            }
        });
    }

    @Override
    public void startMonitoringNetworkStatus() {
        mNetworkStatusManager.register(MyApplication.getInstance(), new NetworkStatusManager.NetworkCallback() {
            @Override
            public void onNetworkAvailable() {
                if (mSongsView == null) return;

                mSongsView.enableRefreshButton();
            }

            @Override
            public void onNetworkUnavailable() {
                if (mSongsView == null) return;
                mSongsView.disableRefreshButton();
            }
        });
    }

    @Override
    public void stopMonitoringNetworkStatus() {
        mNetworkStatusManager.unregister(MyApplication.getInstance());
    }

    @Override
    public void checkNetworkStatus() {
        if (mSongsView == null) return;

        boolean isNetworkAvailable = BaseNetworkStatusManager.isNetworkAvailable(MyApplication.getInstance());
        if (isNetworkAvailable) {
            mSongsView.enableRefreshButton();
        } else {
            mSongsView.disableRefreshButton();
        }
    }
}
