package com.roylaw.app.topmusic.application.di;

import com.roylaw.app.topmusic.utils.network.BaseNetworkStatusManager;
import com.roylaw.app.topmusic.utils.network.NetworkStatusManager_broadcast;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Module(includes = {AppModule.class})
public class NetworkModule {

    @Singleton
    @Provides
    public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Singleton
    @Provides
    public OkHttpClient provideOKHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @Singleton
    @Provides
    public BaseNetworkStatusManager provideNetworkStatusManager() {
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//            return new NetworkStatusManager_networkCallback(application);
//        } else {
//            return new NetworkStatusManager_broadcast();
//        }

        // some bugs found in NetworkStatusManager_networkCallback class
        // use NetworkStatusManager_broadcast for all cases
        return new NetworkStatusManager_broadcast();
    }
}
