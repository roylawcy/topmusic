package com.roylaw.app.topmusic.application.di;

import com.roylaw.app.topmusic.songs.SongsActivity;
import com.roylaw.app.topmusic.songs.SongsModule;
import com.roylaw.app.topmusic.songs.SongsPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, DatabaseModule.class, ApiModule.class, SongsModule.class, NetworkModule.class})
public interface AppComponent {

    // injections
    void inject(SongsActivity songsActivity);
    void inject(SongsPresenter songsPresenter);

}
