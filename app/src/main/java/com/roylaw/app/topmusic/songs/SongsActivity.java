package com.roylaw.app.topmusic.songs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.roylaw.app.topmusic.R;
import com.roylaw.app.topmusic.application.MyApplication;
import com.roylaw.app.topmusic.data.SongsRepository;

import javax.inject.Inject;

public class SongsActivity extends AppCompatActivity {

    SongsPresenter mSongsPresenter;

    @Inject
    SongsRepository mSongsRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs);

        // inject dependencies
        MyApplication.getInstance().getAppComponent().inject(this);

        // get SongsFragment instance
        SongsFragment songsFragment = (SongsFragment) getSupportFragmentManager().findFragmentByTag(SongsFragment.TAG);

        // if SongsFragment is null, init new instance and add to activity
        if (songsFragment == null) {
            songsFragment = SongsFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.root_layout, songsFragment, SongsFragment.TAG)
                    .commit();
        }

        // connect MVP
        mSongsPresenter = new SongsPresenter(mSongsRepository);
        mSongsPresenter.subscribeView(songsFragment);
    }
}
