package com.roylaw.app.topmusic.songs;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.roylaw.app.topmusic.R;
import com.roylaw.app.topmusic.base.BaseViewHolder;
import com.roylaw.app.topmusic.data.Song;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class SongsAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_SONG = 0;

    private List<Song> mSongs;

    public SongsAdapter() {
        mSongs = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            default:
            case VIEW_TYPE_SONG:
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_song, parent, false);
                return new SongsViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SongsViewHolder) {
            Song song = mSongs.get(position);

            // bind data to item
            SongsViewHolder songsViewHolder = (SongsViewHolder) holder;
            songsViewHolder.mNameTextView.setText(song.name);
            songsViewHolder.mArtistNameTextView.setText(song.artistName);

            // load artwork into image view
            RequestOptions options = new RequestOptions()
                    .error(R.drawable.ic_broken_image);
            Glide.with(songsViewHolder.mArtworkImageView)
                    .load(song.artworkUrl)
                    .transition(withCrossFade())
                    .apply(options)
                    .into(songsViewHolder.mArtworkImageView);
        }
    }

    @Override
    public int getItemCount() {
        return mSongs.size();
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_SONG;
    }

    /**
     * Replace new data into adapter.
     * @param songs new song list
     */
    public void replaceData(List<Song> songs) {
        mSongs.clear();
        mSongs.addAll(songs);
        notifyDataSetChanged();
    }

    /**
     * Clear all data.
     */
    public void clearData() {
        mSongs.clear();
        notifyDataSetChanged();
    }

    public class SongsViewHolder extends BaseViewHolder {

        @BindView(R.id.artwork_image_view)
        public ImageView mArtworkImageView;
        @BindView(R.id.name_text_view)
        public TextView mNameTextView;
        @BindView(R.id.artist_name_text_view)
        public TextView mArtistNameTextView;

        public SongsViewHolder(View itemView) {
            super(itemView);
        }
    }
}
