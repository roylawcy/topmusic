package com.roylaw.app.topmusic.utils;

/**
 * Configs class to store all necessary Configs.
 */
public class Configs {
    public static final String API_URL = "https://rss.itunes.apple.com/api/v1/";

    public static final String COUNTRY_HONG_KONG = "hk";

    public static final String MEDIA_TYPE_ITUNES_MUSIC = "itunes-music";

    public static final String FEED_TYPE_TOP_SONGS = "top-songs";

    public static final String GENRE_ALL = "all";

    public static final int RESULTS_LIMIT = 100;

    public static final String ALLOW_EXPLICIT_YES = "explicit";
    public static final String ALLOW_EXPLICIT_NO = "non-explicit";
}
