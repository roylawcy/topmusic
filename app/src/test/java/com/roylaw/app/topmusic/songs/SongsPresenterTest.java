package com.roylaw.app.topmusic.songs;

import android.content.Context;
import android.net.ConnectivityManager;

import com.google.common.collect.Lists;
import com.roylaw.app.topmusic.R;
import com.roylaw.app.topmusic.data.Song;
import com.roylaw.app.topmusic.data.SongsRepository;
import com.roylaw.app.topmusic.data.source.SongsDataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.shadows.ShadowConnectivityManager;
import org.robolectric.shadows.ShadowNetworkInfo;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

@RunWith(RobolectricTestRunner.class)
public class SongsPresenterTest {

    private static List<Song> SONGS = Lists.newArrayList(
            new Song("ATM", "J.Cole", ""),
            new Song("Sugar", "Maroon 5", ""),
            new Song("Grenade", "Bruno Mars", "")
    );

    private SongsPresenter mSongsPresenter;

    private ConnectivityManager mConnectivityManager;
    private ShadowConnectivityManager mShadowConnectivityManager;
    private ShadowNetworkInfo mShadowOfActiveNetworkInfo;

    @Mock
    private SongsRepository mSongsRepository;

    @Mock
    private SongsContract.View mSongsView;

    @Captor
    private ArgumentCaptor<SongsDataSource.GetSongsCallback> mGetSongsCallbackCaptor;

    @Before
    public void setupSongsPresenter() {
        // inject mocks into the test
        MockitoAnnotations.initMocks(this);


        mConnectivityManager =
                (ConnectivityManager)
                        RuntimeEnvironment.application.getSystemService(Context.CONNECTIVITY_SERVICE);
        mShadowConnectivityManager = shadowOf(mConnectivityManager);
        mShadowOfActiveNetworkInfo = shadowOf(mConnectivityManager.getActiveNetworkInfo());


        // get a reference to class under test
        mSongsPresenter = new SongsPresenter(mSongsRepository);
        mSongsPresenter.subscribeView(mSongsView);
    }

    @Test
    public void loadSongsFromRemoteSuccess_loadSongsIntoView() {
        // load songs from remote
        mSongsPresenter.loadSongs(true);
        verify(mSongsView).startLoading();

        // callback is captured and invoked with sample songs
        verify(mSongsRepository).getSongs(eq(true), mGetSongsCallbackCaptor.capture());
        mGetSongsCallbackCaptor.getValue().onSongsLoaded(SONGS);

        // songs are shown in UI
        verify(mSongsView).stopLoading();
        verify(mSongsView).showSongs(SONGS);
        verify(mSongsView).scrollToTop();
        verify(mSongsView).showMessage(R.string.message_refresh_success);
    }

    @Test
    public void loadSongsFromRemoteFail_showErrorMessage() {
        // load songs from remote
        mSongsPresenter.loadSongs(true);
        verify(mSongsView).startLoading();

        // callback is captured and data not available
        verify(mSongsRepository).getSongs(eq(true), mGetSongsCallbackCaptor.capture());
        mGetSongsCallbackCaptor.getValue().onDataNotAvailable();

        // show error message in UI
        verify(mSongsView).stopLoading();
        verify(mSongsView).showMessage(R.string.message_data_not_available);
    }

    @Test
    public void loadSongsFromLocalSuccess_loadSongsIntoView() {
        // load songs from remote
        mSongsPresenter.loadSongs(false);

        // callback is captured and invoked with sample songs
        verify(mSongsRepository).getSongs(eq(false), mGetSongsCallbackCaptor.capture());
        mGetSongsCallbackCaptor.getValue().onSongsLoaded(SONGS);

        // songs are shown in UI
        verify(mSongsView).showSongs(SONGS);
    }

    @Test
    public void loadSongsFromLocalFail_showErrorMessage() {
        // load songs from remote
        mSongsPresenter.loadSongs(false);

        // callback is captured and data not available
        verify(mSongsRepository).getSongs(eq(false), mGetSongsCallbackCaptor.capture());
        mGetSongsCallbackCaptor.getValue().onDataNotAvailable();

        // show error message in UI
        verify(mSongsView).showMessage(R.string.message_data_not_available);
    }

    @Test
    public void networkAvailable_enableButton() {
        // when network is connected
        mShadowOfActiveNetworkInfo.setAvailableStatus(true);

        // check network status
        mSongsPresenter.checkNetworkStatus();

        // enable refresh button
        verify(mSongsView).enableRefreshButton();
    }

    @Test
    public void networkUnavailable_disableButton() {
        // when network is connected
        mShadowOfActiveNetworkInfo.setAvailableStatus(false);

        // check network status
        mSongsPresenter.checkNetworkStatus();

        // disable refresh button
        verify(mSongsView).disableRefreshButton();
    }
}
