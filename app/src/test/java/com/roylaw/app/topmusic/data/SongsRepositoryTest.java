package com.roylaw.app.topmusic.data;

import com.roylaw.app.topmusic.data.source.SongsDataSource;
import com.roylaw.app.topmusic.data.source.local.SongsLocalDataSource;
import com.roylaw.app.topmusic.data.source.remote.SongsRemoteDataSource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

public class SongsRepositoryTest {

    private SongsRepository mSongsRepository;

    @Mock
    private SongsRemoteDataSource mRemoteDataSource;

    @Mock
    private SongsLocalDataSource mLocalDataSource;

    @Mock
    private SongsDataSource.GetSongsCallback mGetSongsCallback;

    @Captor
    private ArgumentCaptor<SongsDataSource.GetSongsCallback> mGetSongsCallbackCaptor;

    @Before
    public void setupSongsRepository() {
        // inject mocks into the test
        MockitoAnnotations.initMocks(this);

        // get a reference to class under test
        mSongsRepository = new SongsRepository(mRemoteDataSource, mLocalDataSource);
    }

    @Test
    public void getSongs_getAllSongsFromRemoteDataSource() {
        // when songs are requested from repository (remote)
        mSongsRepository.getSongs(true, mGetSongsCallback);

        // songs are requested from the remote source
        verify(mRemoteDataSource).getSongs(any(SongsDataSource.GetSongsCallback.class));
    }


    @Test
    public void getSongs_getAllSongsFromLocalDataSource() {
        // when songs are requested from repository (remote)
        mSongsRepository.getSongs(false, mGetSongsCallback);

        // songs are requested from the remote source
        verify(mLocalDataSource).getSongs(any(SongsDataSource.GetSongsCallback.class));
    }

}
