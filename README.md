# README #

Top Music is a sample application to get top music from iTunes.

### Features ###

* Fetch iTunes’ top music and show them in a list with icon, song name, artist name when user clicks a “Refresh” button.
* “Refresh” button should be disabled/enabled based on network connectivity.
* Every time when the list is fetched, store it in a database. Before user clicks the “Refresh” button again, show the locally stored list.

### Libraries Used ###

* Retrofit 2 - API client
* Butter Knife - View binding
* Objectbox - Local database
* Glide - Network image loader
* Dagger 2 - Dependency injection
* okhttp - HTTP client

### Explains ###

* Design with Model-View-Presenter architecture.
* Use Dagger 2 for dependency injection.
* Use Objectbox for local database, as it has great performance and easy to use. (compare with Room, realm etc)
* Use Glide for image loading, as it has better performance in recycler view. (compare with Fresco, Picasso etc)
* The app currently get top musics from iTunes in Hong Kong ("hk"), it can be changed in utils/Configs
* The app use iTunes RSS Feed API to fatch the data.
